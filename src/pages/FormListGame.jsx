import React, { useState } from "react";
import axios from "axios";
import Png from "../img/_.png";
import Batu from "../img/batu.png";
// import Coklat from "../img/coklat.png";
import Gunting from "../img/gunting.png";
import Kertas from "../img/kertas.png";
import Logo from "../img/logo 1.png";
// import Rectangel1 from "../img/Rectangel1.png";
// import Rectangel2 from "../img/Rectangel2.png";
import Refresh from "../img/refresh.png";
import "../css/FormListGame.css";

const FormListGame = () => {
  const [isError, setisError] = useState(false);
  const [result, setResult] = useState(null);

  const submitHandler = (chossen) => {
    axios
      .post("http://localhost:3000/fights", {
        chossen,
      })
      .then((Response) => {
        console.log(Response);
      })
      .catch(() => {
        setisError(true);
      });
    console.log(chossen);
  };

  return (
    <>
      <section className="hal-1">
        {isError ? "server Error" : ""}
        <div className="logo-panah">
          <img src={Png} alt="_" />
        </div>
        <div className="logo">
          <img src={Logo} alt="logo" />
        </div>
        <div>
          <h5>ROCK PAPER SCISSORS</h5>
        </div>
        <div className="playerlogo">
          <p>PLAYER 1</p>
        </div>
        <div className="com">
          <p>com</p>
        </div>
        <div className="player">
          <div id="batu">
            <img onClick={() => submitHandler("batu")} src={Batu} alt="batu" />
          </div>
          <div id="kertas">
            <img
              onClick={() => submitHandler("kertas")}
              src={Kertas}
              alt="kertas"
            />
          </div>
          <div id="gunting">
            <img
              onClick={() => submitHandler("gunting")}
              src={Gunting}
              alt="gunting"
            />
          </div>
        </div>
        <div>
          <div>
            <p id="kalkulasi" className="vs">
              vs
              {result ? result : ""}
            </p>
          </div>
        </div>
        <div className="com">
          <div className="player-com">
            <div className="batu2">
              <img src={Batu} alt="batu" />
            </div>
            <div className="kertas2">
              <img src={Kertas} alt="kertas" />
            </div>
            <div className="gunting2">
              <img src={Gunting} alt="gunting" />
            </div>
          </div>
        </div>
        <div id="refresh">
          <img src={Refresh} alt="refresh" onClick={() => {window.location.reload() }} />
        </div>
      </section>
    </>
  );
};

export default FormListGame;
