import React, { useState } from "react";
import axios from "axios";
import { Form, Row, Col, Button, Alert } from "react-bootstrap";
import "../css/FormLogin.css";

const FormRegister = () => {
  const [inputData, setInputData] = useState({
    username: "",
    password: "",
  });

  const [isError, setIsError] = useState(false);

  const inputhandler = (e) => {
    setInputData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };
  const submitHandler = () => {
    //request api Login
    const endoint = "http://localhost:3000/login";
    axios
      .post(endoint, {
        username: inputData.username,
        passwrd: inputData.password,
      })
      .then(() => {
        if (Response.success) {
          console.log(Response.data);
          // Router.redirec("fight room");
        }
      })
      .catch((err) => {
        setIsError(true);
      });
  };

  return (
    <div>
      {isError ? <Alert variant="danger"> Gagal Login</Alert> : ""}
      <h5 className="form-h5"> Form Login </h5>
      <div className="Container">
        <div className="Container-Form">
          <Form>
            <div className="form-LOgin">
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label>User Name</Form.Label>
                <Form.Control
                  type="text"
                  name="username"
                  value={inputData.username}
                  onChange={inputhandler}
                  placeholder="User Name"
                />
              </Form.Group>
              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="3">
                  Password
                </Form.Label>
                <Col sm="10">
                  <Form.Control
                    type="password"
                    name="password"
                    value={inputData.password}
                    onChange={inputhandler}
                    placeholder="Password"
                  />
                </Col>
              </Form.Group>
              <Button variant="primary" onClick={submitHandler}>
                Submit
              </Button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default FormRegister;
